#include <stddef.h>
#include <stdlib.h>

int cmp(void *a, void *b) {
  return *(int *)a - *(int *)b;
}

int find_odd (size_t length, const int array[length]) {
  
  qsort(array, length, sizeof array[0], cmp);
  
  size_t i = 0;
  int n = array[i];
  int count = 0;
  while (i < length) {
    if (array[i] == n) {
      count++;
    } else if(count % 2 == 1) {
      return n;
    } else {
      // new candidate
      n = array[i];
      count = 1;
    }
    i++;
  }
  
  return n;
}